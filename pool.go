package status

import (
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"sync"
)

// test is turned on when doing tests, it makes several things no-ops.
// output is disabled
// workers are not started
var test = false

// Job desc
type Job func(Line)

// Pool represents a worker pool with managed output
type Pool struct {
	out    io.Writer      // output for jobs
	mu     sync.Mutex     // only one method on Pool will be executed at a time
	jobs   chan Job       // send jobs to the workers
	size   int            // number of workers
	wg     sync.WaitGroup // waitgroup for the workers
	closed bool           // a closed pool cannot be used
}

func (p *Pool) methodStart() func() {
	if p == nil {
		panic("attempt to use a nil pool")
	}

	p.mu.Lock()
	if p.closed {
		panic("pool is closed")
	}

	return p.mu.Unlock
}

// Close closes the workerpool, it is unusable afterwards.
func (p *Pool) Close() {
	defer p.methodStart()()

	close(p.jobs)
	p.wg.Wait()
	p.closed = true
	fmt.Fprint(p.out, moveToLine(p.size+1)+showCursor)
}

// Exec executes a job on the worker pool, it returns once the job
// has started being executed.
func (p *Pool) Exec(job Job) {
	defer p.methodStart()()

	p.jobs <- job
}

// NewPool desc
func NewPool(size int, out io.Writer) *Pool {
	if out == nil {
		out = os.Stderr
	}
	if test {
		out = ioutil.Discard
	}
	fmt.Fprint(out, hideCursor)

	jobs := make(chan Job)
	wg := sync.WaitGroup{}

	// don't spawn workers if this is a test
	if !test {
		wg.Add(size)
		for i := 1; i <= size; i++ {
			go func(line Line) {
				defer wg.Done()
				for job := range jobs {
					job(line)
				}
			}(Line{
				Line: i,
				Out:  out,
			})
		}
	}

	return &Pool{
		jobs: jobs,
		size: size,
		out:  out,
	}
}
