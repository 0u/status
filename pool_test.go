package status

import (
	"io"
	"testing"
)

func init() { test = true }

// TODO: add tests for methodStart locking and unlocking the mutex

func TestPool(t *testing.T) {
	// NOTE(0u): size and out are checked for consistency, nothing more.
	type args struct {
		size int
		out  io.Writer
	}

	tests := []struct {
		name string
		args args

		// assersions contains a function that checks
		// that the output is expected for the args.
		assersions func(t *testing.T, pool *Pool)
	}{
		{
			// TODO: Also assert that Close called wg.Wait()
			"Close should set p.closed and close the jobs channel",
			args{10, nil},
			func(t *testing.T, pool *Pool) {
				pool.Close()

				if pool.closed == false {
					t.Fatal("pool.closed is false, it should be true")
				}

				if _, ok := <-pool.jobs; ok {
					t.Logf("%#v", pool.jobs)
					t.Fatalf("pool.jobs is not closed")
				}
			},
		},
		{
			"Exec should send a function over the jobs channel",
			args{4, nil},

			func(t *testing.T, pool *Pool) {
				called := false
				f := func(Line) { called = true }

				// Add a buffer so we don't need to deal with concurrency in the test
				pool.jobs = make(chan Job, 1)

				pool.Exec(f)

				got := <-pool.jobs
				got(Line{})
				if called == false {
					t.Logf("got = %#v", got)
					t.Fatalf("called is false after calling received function")
				}
			},
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(t *testing.T) {
			pool := NewPool(tc.args.size, tc.args.out)

			// things that should always be true
			if pool.jobs == nil {
				t.Logf("pool = %#v", pool)
				t.Logf("args = %#v", tc.args)
				t.Fatalf("pool.jobs channel is nil")
			}

			if pool.out == nil {
				t.Fatal("pool.out is nil")
			}

			if pool.size != tc.args.size {
				t.Fatalf("want size %d, got size %d", tc.args.size, pool.size)
			}

			// call the extra assersions
			tc.assersions(t, pool)
		})
	}
}

func TestMethodStartPanics(t *testing.T) {
	tests := []struct {
		name        string
		pool        *Pool
		shouldPanic bool
	}{
		{
			"It should panic when closed",
			&Pool{closed: true},
			true,
		},

		{
			"It should not panic when not closed",
			&Pool{closed: false},
			false,
		},

		{
			"It should panic when the pool is nil",
			nil,
			true,
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(t *testing.T) {
			var e interface{}

			func() {
				defer func() { e = recover() }()
				tc.pool.methodStart()
			}()
			t.Logf("e = %#v", e)

			paniced := (e != nil)
			if tc.shouldPanic == true && paniced == false {
				t.Fatalf("It should have paniced, but it did not.")
			}

			if paniced == true && tc.shouldPanic == false {
				t.Fatalf("It should not have paniced, but it did.")
			}
		})
	}
}
