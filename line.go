package status

import (
	"fmt"
	"io"
)

const (
	esc         = "\x1b"
	clearLine   = esc + "[2K"
	clearScreen = esc + "[2J"

	showCursor = esc + "[?25h"
	hideCursor = esc + "[?25l"
)

// Line represents a line on the terminal
type Line struct {
	Line int

	Out io.Writer
}

func moveToLine(line int) string {
	return esc + fmt.Sprintf("[%d;0H", line)
}

func (l Line) Write(b []byte) (int, error) {
	return io.WriteString(l.Out,
		moveToLine(l.Line)+clearLine+string(b))
}

// Printf is a convenince method for writing to a line.
func (l Line) Printf(s string, a ...interface{}) {
	fmt.Fprintf(l, s, a...)
}
