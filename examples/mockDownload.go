package main

import (
	"math/rand"
	"time"

	"github.com/UlisseMini/status"
)

func init() { rand.Seed(time.Now().UnixNano()) }

func main() {
	pool := status.NewPool(30, nil)

	for i := 0; i < 100; i++ {
		pool.Exec(func(l status.Line) {
			l.Printf("[goroutine %d] Downloading %s", l.Line, randomLink())
			time.Sleep(time.Millisecond * time.Duration(rand.Intn(3000)))
		})
	}

	pool.Close()
}

const alphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"

func randomLink() string {
	const site = "example.com"
	extensions := [...]string{"jpg", "jpeg", "mp4", "mp3", "png", "html", "js"}

	ext := extensions[rand.Intn(len(extensions))]

	b := make([]byte, 8)
	for i := range b {
		b[i] = alphabet[rand.Intn(len(alphabet))]
	}
	return "https://" + site + "/" + string(b) + "." + ext
}
