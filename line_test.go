package status

import (
	"bytes"
	"fmt"
	"testing"
)

func TestLine(t *testing.T) {
	for i := 0; i < 10; i++ {
		buf := bytes.NewBuffer(nil)

		line := Line{i, buf}
		line.Printf("Foo! %d", i)

		want := moveToLine(i) + clearLine + fmt.Sprintf("Foo! %d", i)
		if buf.String() != want {
			t.Fatalf("want %q; got %q", want, buf.String())
		}
	}
}
